import 'package:flutter/material.dart';
import 'login.dart' as LoginPage;

class LogPage extends StatefulWidget {
  @override
  _LogPageState createState() => new _LogPageState();
}

class _LogPageState extends State<LogPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(    
      body: new Container(
        child: new ListView(
          children: <Widget>[
            new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Center(
                  child: new LoginPage.LoginPage(),
                )
              ],
            ),
          ],
        ),
        decoration: new BoxDecoration(
          color: const Color(0xff7c94b6),
          image: new DecorationImage(
            fit: BoxFit.cover,
            // colorFilter: new ColorFilter.mode(Colors.white.withOpacity(0), BlendMode.dstATop),
            image: new AssetImage("assets/img_login.jpeg")
          ),
        ),
      ),
    );
  }
}