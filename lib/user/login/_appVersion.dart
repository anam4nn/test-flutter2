import 'package:flutter/material.dart';

class AppVersion extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        new InkWell(
          child: new Text("Buat Akun Baru"),
        ),
        new Padding(
          padding: const EdgeInsets.only(top: 10.0),
          child: new Text("App Version 1.0"),
        )
      ],
    );
  }
}