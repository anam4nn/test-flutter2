import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'dart:async';

final FirebaseAuth auth = FirebaseAuth.instance;
final GoogleSignIn googleSignIn = new GoogleSignIn();

Future<FirebaseUser> _signIn() async{
  GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
  GoogleSignInAuthentication googleSignInAuthentication = await googleSignInAccount.authentication;

  FirebaseUser user = await auth.signInWithGoogle(
    idToken: googleSignInAuthentication.idToken,
    accessToken: googleSignInAuthentication.accessToken
  );
  return user;
}

class LoginPage extends StatelessWidget {
  
  final loginGoogle = Padding(
    padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 10.0, top: 10.0),
    child: new Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Material(
          borderRadius: BorderRadius.circular(30.0),
          shadowColor: Colors.red,
          elevation: 5.0,      
          child: MaterialButton(        
            minWidth: 200.0,
            height: 42.0,
            onPressed: () {
              _signIn();
            },
            color: Colors.red,
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Padding(
                  padding: const EdgeInsets.only(right: 10.0),
                  child: new Image.asset("assets/google.png", height: 32.0, color: Colors.white,),
                ),
                new Text('Masuk Dengan Google', style: TextStyle(color: Colors.white))
              ],
            ),
          ),
        ),
      ],
    )
  );

  final loginFacebook = Padding(
    padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 10.0),
    child: new Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Material(
          borderRadius: BorderRadius.circular(30.0),
          shadowColor: Colors.blue,
          elevation: 5.0,      
          child: MaterialButton(        
            minWidth: 200.0,
            height: 42.0,
            onPressed: null,
            color: Colors.blue,
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Padding(
                  padding: const EdgeInsets.only(right: 10.0),
                  child: new Image.asset("assets/facebook.png", height: 32.0, color: Colors.white,),
                ),
                new Text('Masuk Dengan Facebook', style: TextStyle(color: Colors.white))
              ],
            ),
          ),
        ),
      ],
    )
  );

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown
    ]);
    return new Column(
      children: <Widget>[
        new Image.asset("assets/logo.png", height: 150.0,),
        new Container(
          margin: const EdgeInsets.all(20.0),
          padding: const EdgeInsets.all(20.0),      
          decoration: new BoxDecoration(  
            color: Colors.white.withOpacity(0.2),
            borderRadius: new BorderRadius.circular(10.0)
          ),
          child: new FormLogin(),
        ),
        new Text("atau", style: new TextStyle(color: Colors.white70),),
        loginGoogle,
        loginFacebook,
      ],
    );
  }
}

class FormLogin extends StatelessWidget {
  
  final email = TextFormField(
    keyboardType: TextInputType.emailAddress,      
    style: new TextStyle(color: Colors.white),    
    decoration: new InputDecoration(
      icon: new Icon(Icons.email),
      hintText: 'your@email.com',
      labelText: 'E-mail Address',
      labelStyle: new TextStyle(color: Colors.white),
      hintStyle: new TextStyle(color: Colors.grey[300]),      
    ),
    validator: (value) {
      if (value.isEmpty) {
        return 'Silahkan Masukkan Email Terlebih Dahulu.';
      }
    },
  );

  final username = TextFormField(
    obscureText: true,
    style: new TextStyle(color: Colors.white),
    decoration: new InputDecoration(
      icon: new Icon(Icons.input),
      hintText: 'your password',
      labelText: 'Password',
      labelStyle: new TextStyle(color: Colors.white),
      hintStyle: new TextStyle(color: Colors.grey[300]),
    ),
    validator: (value) {
      if (value.isEmpty) {
        return 'Silahkan Masukkan Password Anda.';
      }
    },
  );

  @override
  Widget build(BuildContext context) {
    final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

    return new Form(
      key: _formKey,
      child: new Column(
        children: <Widget>[
          email,
          SizedBox(height: 20.0,),
          username,
          SizedBox(height: 20.0,),
          new Padding(
            padding: EdgeInsets.symmetric(vertical: 16.0),
            child: Material(
              borderRadius: BorderRadius.circular(5.0),
              shadowColor: Colors.grey[850],
              elevation: 1.0,
              child: MaterialButton(
                minWidth: 200.0,
                height: 42.0,        
                color: Colors.grey[850],
                child: Text('Masuk', style: TextStyle(color: Colors.white)),
                onPressed: () {
                  if (_formKey.currentState.validate()) {
                    // If the form is valid, we want to show a Snackbar
                    Scaffold.of(context).showSnackBar(
                        new SnackBar(content: new Text('Processing Data')));
                  }
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}